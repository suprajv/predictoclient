﻿using System.Web;
using System.Web.Optimization;

namespace DataSematics
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery.min.js",
                        
                        "~/Scripts/multiselect.js",
                        "~/Scripts/bootstrap_table.js",
                        "~/Scripts/moment.min.js",
                        "~/Scripts/hotkey.js",
                        "~/Scripts/chart.min.js",
                        "~/Scripts/jquery.sparkline.min.js",
                        "~/Scripts/custom.js",
                        "~/Scripts/curvedLines.js",
                        "~/Scripts/date.js",
                        "~/Scripts/jquery.flot.js",
                        "~/Scripts/jquery.flot.orderBars.js",
                        "~/Scripts/jquery.flot.pie.js",
                        "~/Scripts/jquery.flot.resize.js",
                        "~/Scripts/jquery.flot.spline.js",
                        "~/Scripts/jquery.flot.stack.js",
                        "~/Scripts/jquery.flot.time.min.js",
                        "~/Scripts/jquery.smartWizard.js",
                        "~/Scripts/pace.min.js",
                        "~/Scripts/icheck.min.js",
                        "~/Scripts/fastclick.js",
                        "~/Scripts/toaster.js",
                        "~/Scripts/jquery.unobtrusive-ajax.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                       
                       "~/Content/mdb.min.css",    
                      "~/Content/green.css", 
                      "~/Content/bootstrap.min.css",
                      "~/Content/bootstrap_table.css",
                      "~/Content/multipleselect.css",
                      "~/Content/bootstrap-progressbar-3.3.4.min.css",
                      "~/Content/madancustom.css",
                      "~/Content/animate.min.css",
                      "~/Content/jquery-jvectormap-2.0.3.css",
                      "~/Content/floatexamples.css",
                      "~/Content/custom.css",
                      "~/Content/toaster.css",
                      "~/Content/nprogress.css"));
        }
    }
}
