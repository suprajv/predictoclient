﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization.Json;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Net;
using System.IO;
using System.Drawing;
using Excel;
using Microsoft.Office.Interop.Excel;
using System.Xml;
using System.Configuration;
using System.Reflection;
using System.Data;
using Models;

namespace DataSematics.General
{
    public class Global
    {
        public static string MainLink = Convert.ToString(ConfigurationManager.AppSettings["MainLink"]);

        public static void getExcelToPDF(string ExcelPath, string PDFPath)
        {
            Microsoft.Office.Interop.Excel.Application ExcelApp;
            Workbooks ExcelWorkbooks = null;
            Workbook ExcelWorkbook = null;
            try
            {
                object misValue = System.Reflection.Missing.Value;
                ExcelApp = new Microsoft.Office.Interop.Excel.Application();
                ExcelApp.Visible = false;
                object varMissing = Type.Missing;
                ExcelWorkbooks = ExcelApp.Workbooks;

                //if file already exist then delete the file
                if (System.IO.File.Exists(PDFPath))
                {
                    System.IO.File.Delete(PDFPath);
                }
                ExcelWorkbook = ExcelWorkbooks.Open(ExcelPath, misValue, misValue,
                                                        misValue, misValue, misValue, misValue, misValue, misValue,
                                                        misValue, misValue, misValue, misValue, misValue, misValue);
                ExcelWorkbook.ExportAsFixedFormat(XlFixedFormatType.xlTypePDF, PDFPath,
                                                    XlFixedFormatQuality.xlQualityStandard,
                                                    varMissing, false, varMissing, varMissing, false, varMissing);
                ExcelWorkbooks.Close();
                ExcelApp.Quit();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ExcelApp = null;
            }
        }

        public static System.Data.DataTable[] ExcelToTable(byte[] FileBuffer, string FilePath)
        {
            try
            {
                List<System.Data.DataTable> olist = new List<System.Data.DataTable>();
                //ExcelDataReader works on binary excel file
                Stream stream = new MemoryStream(FileBuffer);
                //We need to written the Interface.
                IExcelDataReader reader = null;
                if (FilePath.Contains(".xlsx"))
                {
                    //reads the excel file with .xls extension
                    reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                }
                else if (FilePath.Contains(".xls"))
                {
                    //reads excel file with .xlsx extension
                    reader = ExcelReaderFactory.CreateBinaryReader(stream);
                }
                //treats the first row of excel file as Coluymn Names
                reader.IsFirstRowAsColumnNames = true;
                //Adding reader data to DataSet()
                var result = reader.AsDataSet();
                reader.Close();
                //Sending result data to View
                //  getExcelToPDF(Server.MapPath("~/images/blood-sugar-chart.xls"), Server.MapPath("~/images"));
                foreach (System.Data.DataTable dtbl in result.Tables)
                {
                    olist.Add(dtbl);
                }
                return olist.ToArray();
            }
            catch
            {
                return null;
            }
        }

        public static Microsoft.Office.Interop.Excel.Workbook GetExcel(ScoringData[] obj)
        {
            Microsoft.Office.Interop.Excel.Application excel;
            Microsoft.Office.Interop.Excel.Workbook worKbooK;
            Microsoft.Office.Interop.Excel.Worksheet worKsheeT;
            Microsoft.Office.Interop.Excel.Range celLrangE;
            try
            {
                excel = new Microsoft.Office.Interop.Excel.Application();
                excel.Visible = false;
                excel.DisplayAlerts = false;
                worKbooK = excel.Workbooks.Add(Type.Missing);


                worKsheeT = (Microsoft.Office.Interop.Excel.Worksheet)worKbooK.ActiveSheet;
                worKsheeT.Name = "StudentRepoertCard";

                worKsheeT.Range[worKsheeT.Cells[1, 1], worKsheeT.Cells[1, 8]].Merge();
                worKsheeT.Cells[1, 1] = "Student Report Card";
                worKsheeT.Cells.Font.Size = 15;


                int rowcount = 2;
                var odata = GetDataTableFromObjects(obj);

                foreach (DataRow datarow in odata.Rows)
                {
                    rowcount += 1;
                    for (int i = 1; i <= odata.Columns.Count; i++)
                    {

                        if (rowcount == 3)
                        {
                            worKsheeT.Cells[2, i] = odata.Columns[i - 1].ColumnName;
                            worKsheeT.Cells.Font.Color = System.Drawing.Color.Black;

                        }

                        worKsheeT.Cells[rowcount, i] = datarow[i - 1].ToString();

                        if (rowcount > 3)
                        {
                            if (i == odata.Columns.Count)
                            {
                                if (rowcount % 2 == 0)
                                {
                                    celLrangE = worKsheeT.Range[worKsheeT.Cells[rowcount, 1], worKsheeT.Cells[rowcount, odata.Columns.Count]];
                                }

                            }
                        }

                    }

                }

                celLrangE = worKsheeT.Range[worKsheeT.Cells[1, 1], worKsheeT.Cells[rowcount, odata.Columns.Count]];
                celLrangE.EntireColumn.AutoFit();
                Microsoft.Office.Interop.Excel.Borders border = celLrangE.Borders;
                border.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                border.Weight = 2d;

                celLrangE = worKsheeT.Range[worKsheeT.Cells[1, 1], worKsheeT.Cells[2, odata.Columns.Count]];

                return worKbooK;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static System.Data.DataTable GetDataTableFromObjects(dynamic objects)
        {
            if (objects != null && objects.Length > 0)
            {
                Type t = objects[0].GetType();
                System.Data.DataTable dt = new System.Data.DataTable(t.Name);
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    dt.Columns.Add(new DataColumn(pi.Name));
                }
                foreach (var o in objects)
                {
                    DataRow dr = dt.NewRow();
                    foreach (DataColumn dc in dt.Columns)
                    {
                        dr[dc.ColumnName] = o.GetType().GetProperty(dc.ColumnName).GetValue(o, null);
                    }
                    dt.Rows.Add(dr);
                }
                return dt;
            }
            return null;
        }
    }
}