﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DataSematics.Startup))]
namespace DataSematics
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
