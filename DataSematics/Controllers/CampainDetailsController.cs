﻿using BussinesLogic;
using Models;
using Syncfusion.XlsIO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DataSematics.Controllers
{
    public class CampainDetailsController : Controller
    {
        //
        // GET: /CamplainDetails/
        public ActionResult Index(string Id)
        {
            if (Id != null)
            {
                CampainBLL obll = new CampainBLL();
                var odata = obll.Get_Campaign_details(Id);
                return View(odata);
            }
            return RedirectToAction("Index", "Campains");
        }

        public ActionResult Rescore(string Id)
        {
            SuperAdminBLL obll = new SuperAdminBLL();
            Rescore obj = new Rescore();
            var odata = obll.Document_List();
            obj.odocument = odata.content.ToArray();
            obj.campaign_id = Id;
            return View(obj);
        }

        public ActionResult submit_Rescore(string campaign_id)
        {
            UploadsViewModel viewModel = Session["Uploads"] as UploadsViewModel;
            if (viewModel != null && viewModel.Uploads != null && viewModel.Uploads.Count() > 0)
            {
                CampainBLL obll = new CampainBLL();
                Add_Documents odata = new Add_Documents();
                List<Documents_type> olist = new List<Documents_type>();
                odata.campaign_id = campaign_id;
                foreach (var item in viewModel.Uploads)
                {
                    olist.Add(new Documents_type { document_type_id = item.DocTypeId, file_name = item.FileName, file_path = item.FilePath });
                }
                odata.odoc = olist;
                var status = obll.Rescore_campaign(odata);
                Session["Uploads"] = null;
                TempData["toaster_success"] = "Documents Submitted for Scoring";
                return RedirectToAction("Index", "CampainDetails", campaign_id);
            }
            else
            {
                TempData["toaster_error"] = "Please Upload the Documents";
                return RedirectToAction("Index", "CampainDetails", campaign_id);
            }
        }

        public PartialViewResult DrawChart(string Id,string category)
        {
            try
            {
                ViewBag.Message = "Sales";
                CampainBLL obll = new CampainBLL();
                var odata = obll.Get_scoring_summary(Id, category);

                if (odata != null && odata.Count() > 0)
                {
                    List<d_graph_data> obj = new List<d_graph_data>();
                    foreach (var item in odata)
                    {
                        d_graph_data odatas = new d_graph_data();
                        try
                        {
                            odatas.Count = Convert.ToDecimal(item.frequency);
                        }
                        catch(Exception ex)
                        {
                            odatas.Count = 0;
                        }
                        odatas.Product = item.bin;
                        obj.Add(odatas);
                    }
                    return PartialView("_Graph", obj.ToArray());
                }
                else
                {
                    return PartialView("_Graph");
                }
            }
            catch
            {
                return PartialView("_Graph");
            }
        }
        
        public PartialViewResult Campaign_Validation_Stage(string campaign_id)
        {
            SuperAdminBLL SABLL = new SuperAdminBLL();
            var validation_records = SABLL.Get_Campaign_List(campaign_id);
            return PartialView(validation_records);
        }

        public ActionResult Get_cross_sell(string campaign_id)
        {
            try
            {
                CampainBLL obll = new CampainBLL();
                var odata = obll.Get_cross_sell(campaign_id);
                ExcelEngine excelEngine = new ExcelEngine();
                IApplication application = excelEngine.Excel;
                IWorkbook workbook = application.Workbooks.Create(1);
                IWorksheet sheet = workbook.Worksheets[0];
                DataView dv = ((DataTable)General.Global.GetDataTableFromObjects(odata)).DefaultView;
                sheet.ImportDataView(dv, true, 6, 1, -1, -1);
                IStyle headerStyle = workbook.Styles.Add("TableHeading1");
                headerStyle.BeginUpdate();
                workbook.SetPaletteColor(8, System.Drawing.Color.FromArgb(0, 0, 128));
                headerStyle.Color = System.Drawing.Color.FromArgb(0, 0, 128);
                headerStyle.FillBackground = ExcelKnownColors.Light_blue;
                headerStyle.Font.FontName = "Arial";
                sheet.Range["A3:Q3"].Merge();
                sheet.Range["A3:Q3"].Text = "Customer Scoring Prediction";
                sheet.Range["A3:R3"].CellStyle.Font.Size = 20;
                sheet.UsedRange["A6:S6"].VerticalAlignment = ExcelVAlign.VAlignCenter;
                sheet.UsedRange["A6:S6"].HorizontalAlignment = ExcelHAlign.HAlignCenter;
                headerStyle.EndUpdate(); sheet.UsedRange.AutofitRows();
                sheet.UsedRange.AutofitColumns();
                sheet.UsedRange.VerticalAlignment = ExcelVAlign.VAlignTop;
                workbook.SaveAs("" + "CustomerScoringPrediction" + ".xls", ExcelSaveType.SaveAsXLS, System.Web.HttpContext.Current.Response, ExcelDownloadType.PromptDialog);
                excelEngine.ThrowNotSavedOnDestroy = false;
                excelEngine.Dispose();
                return null;
            }
            catch (Exception ex)
            {
                TempData["toaster_error"] = "Document Does Not Exist";
                return RedirectToAction("Index", "CampainDetails", new { Id = campaign_id });
            }
        }

        public ActionResult Get_summary_stat(string campaign_id)
        {
            try
            {
                CampainBLL obll = new CampainBLL();
                var odata = obll.Get_summary_stat(campaign_id);
                ExcelEngine excelEngine = new ExcelEngine();
                IApplication application = excelEngine.Excel;
                IWorkbook workbook = application.Workbooks.Create(1);
                IWorksheet sheet = workbook.Worksheets[0];
                DataView dv = ((DataTable)General.Global.GetDataTableFromObjects(odata)).DefaultView;
                sheet.ImportDataView(dv, true, 6, 1, -1, -1);
                IStyle headerStyle = workbook.Styles.Add("TableHeading1");
                headerStyle.BeginUpdate();
                workbook.SetPaletteColor(8, System.Drawing.Color.FromArgb(0, 0, 128));
                headerStyle.Color = System.Drawing.Color.FromArgb(0, 0, 128);
                headerStyle.FillBackground = ExcelKnownColors.Light_blue;
                headerStyle.Font.FontName = "Arial";
                sheet.Range["A3:Q3"].Merge();
                sheet.Range["A3:Q3"].Text = "Customer Scoring Prediction";
                sheet.Range["A3:R3"].CellStyle.Font.Size = 20;
                sheet.UsedRange["A6:S6"].VerticalAlignment = ExcelVAlign.VAlignCenter;
                sheet.UsedRange["A6:S6"].HorizontalAlignment = ExcelHAlign.HAlignCenter;
                headerStyle.EndUpdate(); sheet.UsedRange.AutofitRows();
                sheet.UsedRange.AutofitColumns();
                sheet.UsedRange.VerticalAlignment = ExcelVAlign.VAlignTop;
                workbook.SaveAs("" + "CustomerScoringPrediction" + ".xls", ExcelSaveType.SaveAsXLS, System.Web.HttpContext.Current.Response, ExcelDownloadType.PromptDialog);
                excelEngine.ThrowNotSavedOnDestroy = false;
                excelEngine.Dispose();
                return null;
            }
            catch (Exception ex)
            {
                TempData["toaster_error"] = "Document Does Not Exist";
                return RedirectToAction("Index", "CampainDetails",new {Id=campaign_id });
            }
        }
    }
}