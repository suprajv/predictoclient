﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Models;
using BussinesLogic;

namespace DataSematics.Controllers
{
    [FilterConfig.SessionAuthorize]
    public class DepartmentController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult Create_Department(department data)
        { 
            SuperAdminBLL SABLL = new SuperAdminBLL();
            var result = SABLL.Create_Department(data);
            var status = result.status;
            if (status == 1)
            {
                TempData["toaster_success"] = "Department Creation Sucessfully";

            }
            else
            {
                TempData["toaster_error"] = "Department already exist";
            }
            return RedirectToAction("Index");

        }

        public ActionResult Delete_department(string Id)
        {

            SuperAdminBLL oBll = new SuperAdminBLL();
            EmptyResponse status = oBll.Delete_department(Id);
            if (status.status == 1)
            {
                TempData["toaster_success"] = "Deleted Sucessfully";

            }
            else
            {
                TempData["toaster_error"] = "Failed to delete";
            }
            return RedirectToAction("department_list", "Department");
        }

        public ActionResult department_list()
        {
            SuperAdminBLL SABLL = new SuperAdminBLL();
            Departments_data data = new Departments_data();
            data = SABLL.department_list(Convert.ToString(Session["User_Id"]));

            return View(data);
        }
	}
}