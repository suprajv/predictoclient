﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BussinesLogic;
using Models;

namespace DataSematics.Controllers
{
    [FilterConfig.SessionAuthorize]
    public class ExperimentController : Controller
    {
        public ActionResult Index()
        {
            SuperAdminBLL SABLL = new SuperAdminBLL();
            Create_Experiment_Type records = SABLL.exp_list(Convert.ToString(Session["User_Id"]));
            return View(records);
        }
        public ActionResult Create()
        {
            SuperAdminBLL SABLL = new SuperAdminBLL();
            List<document_list> result = new List<document_list>();
            insert_exp_type inser_grp = new insert_exp_type();
            document_list records = SABLL.Document_List();
            inser_grp.content = records.content;
            return View(inser_grp);
         
        }
        public ActionResult Insert_exp(insert_exp_type data)
        {
            List<string> olist = new List<string>();

            foreach(var item in data.SelectedValues)
            {
                olist.Add(item);
            }

            Insert_exp obj = new Insert_exp();
            obj.Description = data.Description;
            obj.Name = data.Name;
            obj.Document_id = olist;
            SuperAdminBLL SABLL = new SuperAdminBLL();
            var result = SABLL.Create_Exp(obj);

            if (result.status == 1)
            {
                TempData["toaster_success"] = "Experiment Created Sucessfully";

            }
            else
            {
                TempData["toaster_error"] = "Experiment Creation Failed";
            }
            return RedirectToAction("Create");
        }

        public ActionResult Delete_experiment(string Id)
        {

            SuperAdminBLL oBll = new SuperAdminBLL();
            EmptyResponse status = oBll.Delete_Experiment(Id);
            if (status.status == 1)
            {
                TempData["toaster_success"] = "Deleted Sucessfully";

            }
            else
            {
                TempData["toaster_error"] = "Failed to delete";
            }
            return RedirectToAction("Index", "Experiment");
        }
    }
}