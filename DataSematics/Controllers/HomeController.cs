﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Web.Script.Serialization;
using BussinesLogic;
using Models;

namespace DataSematics.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();
            return View();
        }

        public ActionResult AdminLogin(Login details)
        {
            Account_BLL oaccountBLL = new Account_BLL();
            var oresult = oaccountBLL.UserLogin(details);
            if (oresult.content.user_id != null)
            {
                TempData["toaster_success"] = "Login Successfull";
                Session["User_Id"] = oresult.content.user_id;
                if (oresult.content.role_id == 1)
                {
                    return RedirectToAction("Index", "SuperAdmin");
                }
                else
                {
                    return RedirectToAction("Index", "Dashboard");
                }
            }
            else
            {
                TempData["toaster_error"] = "Invalid UserId or Password";
                return RedirectToAction("Index", "Home");
            }
        }
    }
}