﻿using BussinesLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace pegu.Controllers
{
    public class DataRepresentationController : Controller
    {
        // GET: DataRepresentation
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Segmentation()
        {
            Account_BLL obll = new Account_BLL();
            var odata = obll.Get_BubbleGraph_Filters();
            return View(odata);
        }

        public PartialViewResult Draw_LRChart()
        {
            Account_BLL obll = new Account_BLL();
            var odata = obll.Get_LRGraph_Data();
            return PartialView("_LRGraph", odata);
        }

        public PartialViewResult Draw_NNChart()
        {
            Account_BLL obll = new Account_BLL();
            var odata = obll.Get_NNGraph_Data();
            return PartialView("_NNGraph", odata);
        }

        public PartialViewResult Draw_BubbleChart(string Id)
        {
            Account_BLL obll = new Account_BLL();
            var data = obll.Get_Bubble_Result(Id);
            return PartialView("_BubbleGraph", data);
        }
    }
}