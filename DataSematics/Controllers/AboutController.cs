﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace DataSematics.Controllers
{
    [FilterConfig.SessionAuthorize]
    public class AboutController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}