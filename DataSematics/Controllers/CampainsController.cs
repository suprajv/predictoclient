﻿using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Drawing;
using BussinesLogic;
using DataSematics.General;
using Syncfusion.XlsIO;

namespace DataSematics.Controllers
{
    [FilterConfig.SessionAuthorize]
    public class CampainsController : Controller
    {
        public ActionResult Index()
        {
            CampainBLL obll = new CampainBLL();
            var odata = obll.Get_campains(Convert.ToString(Session["User_Id"]));
            return View(odata);
        }

        public ActionResult AddCampains()
        {
            if (Session["Campain_Details"] == null)
            {
                SuperAdminBLL SABLL = new SuperAdminBLL();
                Campain_insert data = new Campain_insert();
                var records = SABLL.exp_list('1');
                data.content = records.content;
                return View(data);
            }
            else
            {
                SuperAdminBLL SABLL = new SuperAdminBLL();
                Campain_insert data = new Campain_insert();
                var records = SABLL.exp_list('1');
                data.content = records.content;
                var obasicdetails = (Campain_Details)Session["Campain_Details"];
                data.Name = obasicdetails.oBasicDetails.Name;
                data.Description = obasicdetails.oBasicDetails.Description;
                data.Experiment_type_id = obasicdetails.oBasicDetails.Experiment_type_id;
                data.Id = obasicdetails.oBasicDetails.Id;
                return View(data);
            }
        }

        public ActionResult Insert_Campain(Campain_insert data, string id)
        {
            if (Session["Campain_Details"] == null)
            {
                Create_Campain obj = new Create_Campain();
                Campain_Details odetails = new Campain_Details();
                obj.Id = Guid.NewGuid().ToString();
                obj.Description = data.Description;
                obj.Experiment_type_id = data.Experiment_type_id;
                obj.Name = data.Name;
                obj.user_id = Convert.ToString(Session["User_Id"]);
                CampainBLL CBLL = new CampainBLL();
                EmptyResponse status = CBLL.Create_Campain(obj);
                odetails.oBasicDetails = obj;
                Session["Campain_Details"] = odetails;
            }
            else
            {
                var odetails=(Campain_Details)Session["Campain_Details"];
                Create_Campain obj = new Create_Campain();
                obj.Id = odetails.oBasicDetails.Id;
                obj.Description = data.Description;
                obj.Experiment_type_id = data.Experiment_type_id;
                obj.Name = data.Name;
                CampainBLL CBLL = new CampainBLL();
                EmptyResponse status = CBLL.Create_Campain(obj);
                odetails.oBasicDetails = obj;
                Session["Campain_Details"] = odetails;
            }
            return RedirectToAction("Step2");
        }
        
        public ActionResult step2()
        {
            return View();
        }
        
        public ActionResult step3(HttpPostedFileBase file)
        {
            SuperAdminBLL SABLL = new SuperAdminBLL();
            transaction_data result = new transaction_data();
            try
            {
                var odetails=(Campain_Details)Session["Campain_Details"];
                if (odetails.oTransactionDetails == null)
                {
                    List<string> olist = new List<string>();
                    byte[] fileData = null;
                    using (var binaryReader = new BinaryReader(file.InputStream))
                    {
                        fileData = binaryReader.ReadBytes(file.ContentLength);
                    }
                    var odata = Global.ExcelToTable(fileData, file.FileName);
                    int ordinal = -1;
                    foreach (DataTable otable in odata)
                    {
                        foreach (DataColumn column in otable.Columns)
                        {
                            if (column.ColumnName == "CATEGORY")
                            {
                                File1 upload = new File1();

                                var FileName = Guid.NewGuid().ToString() + file.FileName;
                                upload.FilePath = "~/Uploads/" + FileName;
                                file.SaveAs(Server.MapPath(upload.FilePath));
                                var login_id = Convert.ToString(Session["User_Id"]);
                                var SavingPath = "http://" + Global.MainLink + "/Uploads/" + FileName;
                                dynamic status = SABLL.Upload_product_files(SavingPath, login_id);
                                ordinal = column.Ordinal;
                                foreach (DataRow row in otable.Rows)
                                {
                                    foreach (DataColumn col in otable.Columns)
                                    {
                                        if (col.Ordinal == ordinal)
                                        {
                                            if (olist.Contains(Convert.ToString(row[col.ColumnName])))
                                            {
                                            }
                                            else
                                            {
                                                olist.Add(Convert.ToString(row[col.ColumnName]));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    result.osourcelist = olist;
                    result.otargetlist = olist;
                    odetails.oTransactionDetails = result;
                    Session["Campain_Details"] = odetails;

                    return View(result);
                }
                else
                {
                    result.osourcelist = odetails.oTransactionDetails.osourcelist;
                    result.otargetlist = odetails.oTransactionDetails.otargetlist;
                    result.source_id = odetails.oTransactionDetails.source_id;
                    result.target_id = odetails.oTransactionDetails.target_id;
                    result.Id = odetails.oTransactionDetails.Id;

                    return View(result);
                }
            }
            catch
            {
                TempData["toaster_error"] = "Invalid Product hierarchy File";
                result.osourcelist =new  List<string>();
                result.otargetlist = new List<string>();
                return RedirectToAction("step2","Campains");
            }
        }

        public ActionResult Add_transactions(transaction_data data)
        {
            var odetails = (Campain_Details)Session["Campain_Details"];
            if (odetails.oTransactionDetails.source_id == null)
            {
                Create_transaction obj = new Create_transaction();
                CampainBLL obll = new CampainBLL();
                obj.Id = Guid.NewGuid().ToString();
                obj.campain_id = odetails.oBasicDetails.Id;
                obj.source_type = data.source_id;
                obj.target_type = data.target_id;
                var status = obll.Create_transaction(obj);

                odetails.oTransactionDetails.Id = obj.Id;
                odetails.oTransactionDetails.source_id = data.source_id;
                odetails.oTransactionDetails.target_id = data.target_id;
                Session["Campain_Details"] = odetails;
            }
            else
            {

                Create_transaction obj = new Create_transaction();
                CampainBLL obll = new CampainBLL();
                obj.Id = odetails.oTransactionDetails.Id;
                obj.campain_id = odetails.oBasicDetails.Id;
                obj.source_type = data.source_id;
                obj.target_type = data.target_id;
                var status = obll.Create_transaction(obj);
            }

            return RedirectToAction("step4", "Campains");
        }

        public ActionResult step4()
        {
            SuperAdminBLL obll = new SuperAdminBLL();
            var odata = obll.Document_List();
            return View(odata.content.ToArray());
        }

        public ActionResult Add_Campaign_Documents()
        {
            if (Session["Campain_Details"] != null)
            {
                var odetails = (Campain_Details)Session["Campain_Details"];
                UploadsViewModel viewModel = Session["Uploads"] as UploadsViewModel;
                if (viewModel != null && viewModel.Uploads != null && viewModel.Uploads.Count() > 0)
                {
                    CampainBLL obll = new CampainBLL();
                    Add_Documents odata = new Add_Documents();
                    List<Documents_type> olist = new List<Documents_type>();
                    odata.campaign_id = odetails.oBasicDetails.Id;
                    foreach (var item in viewModel.Uploads)
                    {
                        olist.Add(new Documents_type { document_type_id = item.DocTypeId, file_name = item.FileName, file_path = item.FilePath });
                    }
                    odata.odoc = olist;
                    var status = obll.Add_Documents(odata);
                    Session["Campain_Details"] = null;
                    Session["Uploads"] = null;
                    TempData["toaster_success"] = "Campaign Created Successfull";
                    return RedirectToAction("Index", "CampainDetails", new {Id= odetails.oBasicDetails.Id });
                }
                else
                {
                    TempData["toaster_error"] = "Please Upload the Documents";
                    return RedirectToAction("step4", "Campains");
                }
            }
            else
            {
                return RedirectToAction("Index", "Campains");
            }
        }

        public PartialViewResult CampaignCreationSteps(int StepNo)
        {
            Camapin_Steps ostep = new Camapin_Steps();
            List<Document_Type> otype = new List<Document_Type>();
            otype.Add(new Document_Type { Id = "1", Name = "Content Type" });
            otype.Add(new Document_Type { Id = "2", Name = "Product Heirarchy" });
            otype.Add(new Document_Type { Id = "3", Name = "Choose Content" });
            otype.Add(new Document_Type { Id = "4", Name = "Upload Document" });
            otype.Add(new Document_Type { Id = "5", Name = "Complete" });
            ostep.Step = StepNo;
            ostep.otype = otype;
            return PartialView(ostep);
        }

        [HttpPost]
        public ActionResult Upload(FormCollection form, HttpPostedFileBase[] files)
        {
            foreach (var items in files)
            {
                if (items == null)
                {
                    ViewBag.message = "valid err";
                    return View("Index");
                }
            }
            
            List<File1> Uploads = new List<File1>();
            UploadsViewModel uploadsViewModel = Session["Uploads"] != null ? Session["Uploads"] as UploadsViewModel : new UploadsViewModel();
            foreach (HttpPostedFileBase file in files)
            {
                File1 upload = new File1();
                upload.FileID = uploadsViewModel.Uploads.Count + 1;
                upload.FileName = Guid.NewGuid().ToString()+file.FileName;
                upload.DocType = Convert.ToString(form["DOCTYPE"]);
                upload.DocTypeId = Convert.ToString(form["DOCTYPEID"]); 
                upload.FilePath = "http://" + Global.MainLink + "/Uploads/" + upload.FileName;
                file.SaveAs(Server.MapPath("~/Uploads/" + upload.FileName));
                uploadsViewModel.Uploads.Add(upload);
                Session["Uploads"] = uploadsViewModel;
            }

            return PartialView("~/Views/Campains/_UploadsPartial.cshtml", uploadsViewModel.Uploads);

        }

        public ActionResult DeleteFile(long id)
        {
            UploadsViewModel viewModel = Session["Uploads"] as UploadsViewModel;

            File1 file = viewModel.Uploads.Single(x => x.FileID == id);

            try
            {
                viewModel.Uploads.Remove(file);
            }

            catch (Exception)
            {
                return PartialView("~/Views/Campains/_UploadsPartial.cshtml", viewModel.Uploads);
            }

            return PartialView("~/Views/Campains/_UploadsPartial.cshtml", viewModel.Uploads);
        }

        public ActionResult GetFiles()
        {
            UploadsViewModel viewModel = Session["Uploads"] as UploadsViewModel;

            return PartialView("~/Views/Campains/_UploadsPartial.cshtml", (viewModel == null ? new UploadsViewModel().Uploads : viewModel.Uploads));
        }

        public ActionResult Get_Campaign_Excel(string Id)
        {
            try
            {
                CampainBLL obll = new CampainBLL();
                var odata = obll.Get_Campain_Excel(Id);
                ExcelEngine excelEngine = new ExcelEngine();
                IApplication application = excelEngine.Excel;
                IWorkbook workbook = application.Workbooks.Create(1);
                IWorksheet sheet = workbook.Worksheets[0];
                DataView dv = ((DataTable)Global.GetDataTableFromObjects(odata)).DefaultView;
                sheet.ImportDataView(dv, true, 6, 1, -1, -1);
                IStyle headerStyle = workbook.Styles.Add("TableHeading1");
                headerStyle.BeginUpdate();
                workbook.SetPaletteColor(8, System.Drawing.Color.FromArgb(0, 0, 128));
                headerStyle.Color = System.Drawing.Color.FromArgb(0, 0, 128);
                headerStyle.FillBackground = ExcelKnownColors.Light_blue;
                headerStyle.Font.FontName = "Arial";
                sheet.Range["A3:Q3"].Merge();
                sheet.Range["A3:Q3"].Text = "Customer Scoring Prediction";
                sheet.Range["A3:R3"].CellStyle.Font.Size = 20;
                sheet.UsedRange["A6:S6"].VerticalAlignment = ExcelVAlign.VAlignCenter;
                sheet.UsedRange["A6:S6"].HorizontalAlignment = ExcelHAlign.HAlignCenter;
                headerStyle.EndUpdate(); sheet.UsedRange.AutofitRows();
                sheet.UsedRange.AutofitColumns();
                sheet.UsedRange.VerticalAlignment = ExcelVAlign.VAlignTop;
                workbook.SaveAs("" + "CustomerScoringPrediction" + ".xls", ExcelSaveType.SaveAsXLS, System.Web.HttpContext.Current.Response, ExcelDownloadType.PromptDialog);
                excelEngine.ThrowNotSavedOnDestroy = false;
                excelEngine.Dispose();
                return null;
            }
            catch (Exception ex)
            {
                TempData["toaster_error"] = "Document Does Not Exist";
                return RedirectToAction("Index", "Campains");
            }
        }
    }
}