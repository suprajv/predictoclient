﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Web.Helpers;
using BussinesLogic;
using Models;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.Web.Mvc.Html;
//using System.Web.Helpers;
using System.Json;
using System.Web.Script.Serialization;
using System.IO;
using pegu.Controllers;

namespace DataSematics.Controllers
{
    [FilterConfig.SessionAuthorize]
    public class DashboardController : Controller
    {
        public ActionResult Index()
        {
            Account_BLL obll = new Account_BLL();
            var odata = obll.Get_Dashboard_details(Convert.ToString(Session["User_Id"]));

            return View(odata);
        }

        public PartialViewResult DrawChart(string column,string target,int type)
        {
            try
            {
                ViewBag.Message = "Sales";
                Account_BLL obll = new Account_BLL();
                var odata = obll.Get_data_details(column, target,type);

                if (odata != null && odata.Count() > 0)
                {
                    List<d_graph_data> obj = new List<d_graph_data>();
                    foreach (var item in odata)
                    {
                        d_graph_data odatas = new d_graph_data();
                        try
                        {
                            odatas.Count = Convert.ToDecimal(item.frequency);
                        }
                        catch
                        {
                            odatas.Count = 0;
                        }
                        odatas.Product = item.bin;
                        obj.Add(odatas);
                    }
                    if (type == 1)
                    {
                        return PartialView("_BarGraph", obj.ToArray());
                    }
                    else
                    {
                        return PartialView("_Graph", obj.ToArray());
                    }
                }
                else
                {
                    return PartialView("_Graph");
                }
            }
            catch
            {
                return PartialView("_Graph");
            }
        }



        public ActionResult overlap()
        {
            Product_Purchaselist products_data = new Product_Purchaselist();
            Product_purchase_BLL productBll = new Product_purchase_BLL();
            var res = productBll.campain_list();
            products_data.content = products_data.content;
            products_data.content = res;
            return View(products_data);
        }
        public PartialViewResult Drawproduct_overlap(string campain)
        {
            
            //List<Overlap> olist = new List<Overlap>();
            //List<string> o=new List<string>();
            //o.Add("assda");
            //olist.Add(new Overlap { sets = o, size = 10, label = "asd" });
            //return PartialView("Overlap_graph", olist.ToArray());
              try
            {
                ViewBag.Message = "Sales";
                Product_purchase_BLL obll = new Product_purchase_BLL();
                var odata = obll.Product_overlap_data(campain);
               
                if (odata != null && odata.Count() > 0)
                {
                    var rows = new List<Object>();
                    List<g_overlap_graph> obj = new List<g_overlap_graph>();
                    foreach (var item in odata)
                    {
                        g_overlap_graph odatas = new g_overlap_graph();

                        odatas.sets = item.Products; 
                        odatas.label = item.Product_Label;
                        odatas.size = item.size;

                        obj.Add(odatas);

                         
                    }
                    var options = obj;
                    var serializer = new JsonSerializer();
                    var stringWriter = new StringWriter();
                    using (var writer = new JsonTextWriter(stringWriter))
                    {
                        writer.QuoteName = false;
                        serializer.Serialize(writer, options);
                    }
                    var json = stringWriter.ToString();
                    json = json.Replace("\"[", "[").Replace("]\"", "]");

                    return PartialView("Overlap_graph", json);
                     
                }
                else
                {
                    return PartialView("Overlap_graph");
                }
            }
            catch
            {
                return PartialView("Overlap_graph");
            }


        }

        public ActionResult Overlap_graph()
        {
            Product_purchase_BLL productBll = new Product_purchase_BLL();
            return View();
        }

    }
}