﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BussinesLogic;
using Models; 
using System.Web.Helpers;

namespace DataSematics.Controllers
{
    [FilterConfig.SessionAuthorize]
    public class SuperAdminController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DrawChart()
        {

            var myChart = new Chart(width: 500, height: 400)
                .AddTitle("Chart Title")
                .AddSeries(
                    name: "Employee",
                    xValue: new[] { "Peter", "Andrew", "Julie", "Mary", "Dave" },
                    yValues: new[] { "2", "6", "4", "5", "3" })
              .GetBytes("png");
            return File(myChart, "image/bytes");

        }

        public ActionResult DrawChart1()
        {
            var chart = new Chart(width: 500, height: 400)
             .AddTitle("Success Campains")
                .AddSeries(
                            chartType: "Pie",
                            xValue: new[] { "Success", "Failure", "Processing" },
                            yValues: new[] { "60", "10", "10" })
                            .GetBytes("png");
            return File(chart, "image/bytes");
        }  
	}
}