﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BussinesLogic;
using Models;

namespace DataSematics.Controllers
{
    [FilterConfig.SessionAuthorize]
    public class Create_AdminController : Controller
    {
        public ActionResult Index()
        {
            SuperAdminBLL SABLL = new SuperAdminBLL();
            Departments_data data = new Departments_data();
            data = SABLL.department_list(Session["User_Id"].ToString());

            return View(data);
        }
        public ActionResult Create()
        {
            SuperAdminBLL SABLL = new SuperAdminBLL();
            Departments_data data = new Departments_data();
            data = SABLL.department_list(Session["User_Id"].ToString());

            return View(data);
        }

        public ActionResult add_admin(Departments_data data)
        {
            Create_admin obj = new Create_admin();
            obj.department_id = data.admin_create.department_id;
            obj.user_id = data.admin_create.user_id;
            obj.password = data.admin_create.password;
            SuperAdminBLL SABLL = new SuperAdminBLL();
            var result = SABLL.Create_Admin(obj);
            var status = result.status;
            if (status == 1)
            {
                TempData["toaster_success"] = "New Admin Created Sucessfully";

            }
            else
            {
                TempData["toaster_error"] = "Admin already exist";
            }
            return RedirectToAction("Create");
            
        }

        public ActionResult View_users(string Id)
        {
            SuperAdminBLL SABLL = new SuperAdminBLL();
            User_information data = SABLL.Get_UserList(Id);

            return View(data);
        }
	}
}