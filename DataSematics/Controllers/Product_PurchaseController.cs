﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Models;
using BussinesLogic;
namespace pegu.Controllers
{
    public class Product_PurchaseController : Controller
    {
        //
        // GET: /Product_Purchase/
        public ActionResult Index()
        {
            Product_Purchaselist products_data = new Product_Purchaselist();
            Product_purchase_BLL productBll=new Product_purchase_BLL();
            var res = productBll.campain_list();  
            products_data.content = products_data.content;
           
            var odata = productBll.product_list();
            products_data.product_list = odata;
            products_data.content = res;
            return View(products_data);
        }
        public ActionResult _Graph()
        {
            return View();
        }
        public PartialViewResult DrawChart(string campain, string product)
        {
            try
            {
                ViewBag.Message = "Sales";
                Product_purchase_BLL obll = new Product_purchase_BLL();
                var odata = obll.Get_data_details(campain, product);

                if (odata != null && odata.Count() > 0)
                {
                    List<d_graph_data> obj = new List<d_graph_data>();
                    foreach (var item in odata)
                    {
                        d_graph_data odatas = new d_graph_data();
                        try
                        {
                            odatas.Count = Convert.ToDecimal(item.Frequency);
                        }
                        catch
                        {
                            odatas.Count = 0;
                        }
                        odatas.Product = item.Age_Bucket;
                        obj.Add(odatas);
                    }

                    return PartialView("_Graph", obj.ToArray());

                }
                else
                {
                    return PartialView("_Graph");
                }
            }
            catch
            {
                return PartialView("_Graph");
            }
        }

        public ActionResult matrix_view()
        { 
            Product_Purchaselist products_data = new Product_Purchaselist();
            Product_purchase_BLL productBll = new Product_purchase_BLL();
            var res = productBll.campain_list();
            products_data.content = products_data.content;
            var odata = productBll.feature_list();
            products_data.product_list = odata;
            products_data.content = res;
            return View(products_data);
        }


        public ActionResult _matrix_graph()
        {
            return View();
        }

        public PartialViewResult Draw_matrix(string campain, string product)
        {
            try
            {
                ViewBag.Message = "Sales";
                Product_purchase_BLL obll = new Product_purchase_BLL();
                var odata = obll.Get_matrix_view(campain, product);

                if (odata != null && odata.Count() > 0)
                {
                    List<g_matrix_data> obj = new List<g_matrix_data>();
                    foreach (var item in odata)
                    {
                        g_matrix_data odatas = new g_matrix_data();
                        try
                        {
                            odatas.Count = Convert.ToDecimal(item.avg);
                        }
                        catch
                        {
                            odatas.Count = 0;
                        }
                        odatas.Product = item.bin;
                        obj.Add(odatas);
                    }

                    return PartialView("_matrix_graph", obj.ToArray());

                }
                else
                {
                    return PartialView("_matrix_graph");
                }
            }
            catch
            {
                return PartialView("_matrix_graph");
            }
        }
	}
}