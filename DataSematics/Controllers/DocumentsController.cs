﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BussinesLogic;
using Models;
using System.Web.Helpers;

namespace DataSematics.Controllers
{
    [FilterConfig.SessionAuthorize]
    public class DocumentsController : Controller
    {
        public ActionResult Index()
        {
            SuperAdminBLL SABLL = new SuperAdminBLL();
            List<document_list> result = new List<document_list>();
            document_list records = SABLL.Document_List();

            return View(records);
        }
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Create_Document(insert_document data)
        {
            var obj = data.Document_name;
            SuperAdminBLL SABLL = new SuperAdminBLL();
            var result = SABLL.Create_Doc(obj);
            var status = result.status;
            if (status == 1)
            {
                TempData["toaster_success"] = "Document Created Sucessfully";

            }
            else
            {
                TempData["toaster_error"] = "Document Creation Failed";
            }
            return RedirectToAction("Create");

        }

        public ActionResult Delete_Documents_type(string Id)
        {

            SuperAdminBLL oBll = new SuperAdminBLL();
            EmptyResponse status = oBll.Delete_Documents_Type(Id);
            if (status.status == 1)
            {
                TempData["toaster_success"] = "Deleted Sucessfully";

            }
            else
            {
                TempData["toaster_error"] = "Failed to delete";
            }
            return RedirectToAction("Index", "Experiment");
        }
    }
}