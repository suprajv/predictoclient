﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BussinesLogic;
using Models;

namespace DataSematics.Controllers
{
    [FilterConfig.SessionAuthorize]
    public class ProfileController : Controller
    {
        public ActionResult Index()
        {
            Account_BLL PBLL = new Account_BLL();
            User_information_details data = PBLL.User_Information(Convert.ToString(Session["User_Id"]));
            Profile_information obj = new Profile_information();
            foreach(var item in data.content)
            {
                obj.created_by = item.created_by;
                obj.created_date = item.created_date;
                obj.email_id = item.email_id;
                obj.firstname = item.firstname;
                obj.id = item.id;
                obj.lastname = item.lastname;
                obj.login_id = item.login_id;
                obj.modified_by = item.modified_by;
                obj.modified_date = item.modified_date;
                obj.phone_no = item.phone_no;
            }
            return View(obj);
        }


        public ActionResult Edit()
        {
            Account_BLL PBLL = new Account_BLL();
            User_information_details data = PBLL.User_Information(Convert.ToString(Session["User_Id"]));
            Profile_information obj = new Profile_information();
            foreach (var item in data.content)
            {
                obj.created_by = item.created_by;
                obj.created_date = item.created_date;
                obj.email_id = item.email_id;
                obj.firstname = item.firstname;
                obj.id = item.id;
                obj.lastname = item.lastname;
                obj.login_id = item.login_id;
                obj.modified_by = item.modified_by;
                obj.modified_date = item.modified_date;
                obj.phone_no = item.phone_no;
            } 
            return View(obj);
        }

        public ActionResult Update(Profile_information data)
        {
            Insert_user_information obj = new Insert_user_information();
            obj.firstname = data.firstname;
            obj.lastname = data.lastname;
            obj.phone_no = data.phone_no;
            obj.email_id = data.email_id;
            obj.login_id=Session["User_Id"].ToString();
            obj.id = string.Empty;
            obj.created_by = Session["User_Id"].ToString();
            obj.created_date = DateTime.Now.ToString();
            obj.modified_by = Session["User_Id"].ToString();
            obj.modified_date = DateTime.Now.ToString();
            Account_BLL PBLL = new Account_BLL();
            dynamic status = PBLL.Update_Information(obj);
            if (status !=null)
            {
                TempData["toaster_success"] = "Updated Successfully";

            }
            else
            {
                TempData["toaster_error"] = "Failed to Update";
            }
            return RedirectToAction("Edit");
          
        }

        public ActionResult Change_Password()
        {
            return View();
        }

        public ActionResult Check_Passsword(Confirmpassword data)
        {
            Account_BLL PBLL = new Account_BLL();
            var old_password = data.Password;
            var new_Password = data.New_Password;
            var login_id = Session["User_Id"].ToString();
            var status = PBLL.Check_Password(login_id, old_password, new_Password);
            if (status.status != 0)
            {
                TempData["toaster_success"] = "Password Updated successfully";

            }
            else
            {
                TempData["toaster_error"] = "Failed to Update Password";
            }
            return RedirectToAction("Change_Password");
        } 
    }
}