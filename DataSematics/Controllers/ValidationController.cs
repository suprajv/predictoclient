﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BussinesLogic;
using Models;

namespace DataSematics.Controllers
{
    [FilterConfig.SessionAuthorize]
    public class ValidationController : Controller
    {
        public ActionResult Index()
        { 
            SuperAdminBLL SABLL = new SuperAdminBLL();
            List<document_list> result = new List<document_list>();
            document_list records = SABLL.Validation_List();
            return View(records);
        }

        public ActionResult Create()
        {
            return View();   
        }

        public ActionResult validation_Step(insert_document data)
        {
            SuperAdminBLL SABLL = new SuperAdminBLL();
            var result = SABLL.Create_step(data.Id, data.Document_name);
            var status = result.status;
            if (status == 1)
            {
                TempData["toaster_success"] = "Validation Created Sucessfully"; 

            }
            else
            {
                TempData["toaster_error"] = "Validation Creation Failed";
            }
             
            return RedirectToAction("Create");
        }

        public ActionResult Delete_validation(string Id)
        {

            SuperAdminBLL oBll = new SuperAdminBLL();
            EmptyResponse status = oBll.Delete_Validation(Id);
            if (status.status == 1)
            {
                TempData["toaster_success"] = "Deleted Sucessfully";

            }
            else
            {
                TempData["toaster_error"] = "Failed to delete";
            }
            return RedirectToAction("Index","Validation");
        }
    }
}