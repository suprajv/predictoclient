﻿using System;
using System.Configuration;

namespace BussinesLogic
{
    public class General
    {
      public static string Service_Link = Convert.ToString(ConfigurationManager.AppSettings["WebServiceLink"]);
    }
}
