﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Web.Script.Serialization;
using Models;
//using System.Json;
namespace BussinesLogic
{
   public class Product_purchase_BLL
    {

       public Product_Purchase_campains[] campain_list()
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Product_purchase/Get_campains");
                httpWebRequest.Method = "GET";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";
                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(Product_Purchase_campains[]));
                Product_Purchase_campains[] obj = serializer.ReadObject(readStream.BaseStream) as Product_Purchase_campains[];



                return obj;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }


       public Purchase_Products[] product_list()
       {
           try
           {
               StreamReader readStream;
               HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Product_purchase/Get_Data");
               httpWebRequest.Method = "GET";
               httpWebRequest.ContentType = @"application/json; charset=utf-8";
               HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
               readStream = new StreamReader(httpResponse.GetResponseStream());

               var serializer = new DataContractJsonSerializer(typeof(Purchase_Products[]));
               Purchase_Products[] obj = serializer.ReadObject(readStream.BaseStream) as Purchase_Products[];



               return obj;
           }
           catch (Exception e)
           {
               Console.Write(e);
               return null;
           }
       }


       public Purch_Prod_grap_data[] Get_data_details(string column, string target)
       {
           try
           {
               StreamReader readStream;
               HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Product_purchase/Get_Sorted_Result?target=" + target + "&column=" + column);
               httpWebRequest.Method = "GET";
               httpWebRequest.ContentType = @"application/json; charset=utf-8";
               HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
               readStream = new StreamReader(httpResponse.GetResponseStream());

               var serializer = new DataContractJsonSerializer(typeof(Purch_Prod_grap_data[]));
               Purch_Prod_grap_data[] obj = serializer.ReadObject(readStream.BaseStream) as Purch_Prod_grap_data[];

               return obj;
           }
           catch (Exception e)
           {
               Console.Write(e);
               return null;
           }
       }


       public Purchase_Products[] feature_list()
       {
           try
           {
               StreamReader readStream;
               HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Product_purchase/Get_featurelist");
               httpWebRequest.Method = "GET";
               httpWebRequest.ContentType = @"application/json; charset=utf-8";
               HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
               readStream = new StreamReader(httpResponse.GetResponseStream());

               var serializer = new DataContractJsonSerializer(typeof(Purchase_Products[]));
               Purchase_Products[] obj = serializer.ReadObject(readStream.BaseStream) as Purchase_Products[];



               return obj;
           }
           catch (Exception e)
           {
               Console.Write(e);
               return null;
           }
       }

        

       public Matrix_viewdata[] Get_matrix_view(string column, string target)
       {
           try
           {
               StreamReader readStream;
               HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Product_purchase/matrix_graph?target=" + column + "&column=" + target);
               httpWebRequest.Method = "POST";
               httpWebRequest.ContentType = @"application/json; charset=utf-8";
               httpWebRequest.ContentLength = 0;

               HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
               readStream = new StreamReader(httpResponse.GetResponseStream());

               var serializer = new DataContractJsonSerializer(typeof(Matrix_viewdata[]));

               Matrix_viewdata[] obj = serializer.ReadObject(readStream.BaseStream) as Matrix_viewdata[];

               return obj;
           }
           catch (Exception ex)
           {
               return null;
           }
       }


       public product_overlap[] Product_overlap_data(string column)
       {
           try
           {
               StreamReader readStream;
               HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Product_purchase/product_overlapdata?target=" + column);
               httpWebRequest.Method = "POST";
               httpWebRequest.ContentType = @"application/json; charset=utf-8";
               httpWebRequest.ContentLength = 0;
               
               HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
               readStream = new StreamReader(httpResponse.GetResponseStream());

               var serializer = new DataContractJsonSerializer(typeof(product_overlap[]));
                
               product_overlap[] obj = serializer.ReadObject(readStream.BaseStream) as product_overlap[];
              // return Json(obj, JsonRequestBehavior.AllowGet);
            
               return obj;
           }
           catch (Exception ex)
           {
               return null;
           }
       }


    }
}
