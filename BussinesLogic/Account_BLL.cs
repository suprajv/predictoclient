﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Web.Script.Serialization;
using Models;

namespace BussinesLogic
{
   public  class Account_BLL
    {
       public Session_values UserLogin(Login userdetails)
       {
           try
           {
               StreamReader readStream;
               HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Login/UserLogin");
               httpWebRequest.Method = "POST";
               httpWebRequest.ContentType = @"application/json; charset=utf-8";

               using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
               {
                   string json = new JavaScriptSerializer().Serialize(userdetails);

                   streamWriter.Write(json);
               }

               HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
               readStream = new StreamReader(httpResponse.GetResponseStream());

               var serializer = new DataContractJsonSerializer(typeof(Session_values));

               Session_values obj = serializer.ReadObject(readStream.BaseStream) as Session_values;

               return obj;
           }
           catch (Exception ex)
           {
               return null;
           }
       }

        public Dashboard Get_Dashboard_details(string user_id)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Login/Get_Dashboard_Details?user_id="+user_id);
                httpWebRequest.Method = "GET";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";
                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(Dashboard_Details));
                Dashboard_Details obj = serializer.ReadObject(readStream.BaseStream) as Dashboard_Details;

                return obj.content;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }

        public User_information_details User_Information(string Login_id)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Profile/Get_User_Details/" + "?Login_id=" + Login_id);
                httpWebRequest.Method = "GET";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";
                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(User_information_details));
                User_information_details obj = serializer.ReadObject(readStream.BaseStream) as User_information_details;



                return obj;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }

        public EmptyResponse Update_Information(Insert_user_information Name)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Profile/Add_User_Information");
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(Name);

                    streamWriter.Write(json);
                }

                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(EmptyResponse));

                EmptyResponse obj = serializer.ReadObject(readStream.BaseStream) as EmptyResponse;

                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public EmptyResponse Check_Password(string Login_id, string old_password, string new_password)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Profile/Change_Password/" + "?Login_id=" + Login_id + "&old_password=" + old_password + "&new_password=" + new_password);
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(Login_id);

                    streamWriter.Write(json);
                }

                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(EmptyResponse));

                EmptyResponse obj = serializer.ReadObject(readStream.BaseStream) as EmptyResponse;

                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Visual_Data[] Get_data_details(string column,string target,int type)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Login/Get_Sorted_Result?target="+target+"&column="+column + "&type=" + type);
                httpWebRequest.Method = "GET";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";
                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(Visual_Data[]));
                Visual_Data[] obj = serializer.ReadObject(readStream.BaseStream) as Visual_Data[];

                return obj;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }

        public Line_Data[] Get_LRGraph_Data()
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Login/Get_LR_Result");
                httpWebRequest.Method = "GET";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";
                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(Line_Data[]));
                Line_Data[] obj = serializer.ReadObject(readStream.BaseStream) as Line_Data[];

                return obj;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }


        public Line_Data[] Get_NNGraph_Data()
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Login/Get_NN_Result");
                httpWebRequest.Method = "GET";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";
                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(Line_Data[]));
                Line_Data[] obj = serializer.ReadObject(readStream.BaseStream) as Line_Data[];

                return obj;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }


        public Bubble_Filters Get_BubbleGraph_Filters()
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Login/Get_Bubble_Filters");
                httpWebRequest.Method = "GET";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";
                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(Bubble_Filters));
                Bubble_Filters obj = serializer.ReadObject(readStream.BaseStream) as Bubble_Filters;

                return obj;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }


        public Line_Data[] Get_Bubble_Result(string Id)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Login/Get_Bubble_Result?filter="+Id);
                httpWebRequest.Method = "GET";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";
                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(Line_Data[]));
                Line_Data[] obj = serializer.ReadObject(readStream.BaseStream) as Line_Data[];

                return obj;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }
    }
}
