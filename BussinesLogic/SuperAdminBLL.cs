﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Web.Script.Serialization;
using Models;

namespace BussinesLogic
{
    public class SuperAdminBLL
    {
        public EmptyResponse Create_Doc(string Name)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/master/Add_Document_Type/" + "?Name=" + Name);
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(Name);

                    streamWriter.Write(json);
                }

                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(EmptyResponse));

                EmptyResponse obj = serializer.ReadObject(readStream.BaseStream) as EmptyResponse;

                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public document_list Validation_List()
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/master/GetValidation_status");
                httpWebRequest.Method = "GET";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";
                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(document_list));
                document_list obj = serializer.ReadObject(readStream.BaseStream) as document_list;



                return obj;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }


        public Campaign_Validation_Stage Get_Campaign_List(string campaign_id)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/master/Get_Campaign_stages?campaign_Id=" + campaign_id);
                httpWebRequest.Method = "GET";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";
                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(Campaign_Validation_Stage));
                Campaign_Validation_Stage obj = serializer.ReadObject(readStream.BaseStream) as Campaign_Validation_Stage;

                return obj;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }

        public document_list Document_List()
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/master/Get_Document_Types");
                httpWebRequest.Method = "GET";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";
                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(document_list));
                document_list obj = serializer.ReadObject(readStream.BaseStream) as document_list;



                return obj;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }

        public EmptyResponse Create_step(string Id,string Name)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/master/Add_Validation_status/"+Id+ "?Name=" + Name);
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(Name);

                    streamWriter.Write(json);
                }

                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(EmptyResponse));

                EmptyResponse obj = serializer.ReadObject(readStream.BaseStream) as EmptyResponse;

                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public EmptyResponse Create_Exp(Insert_exp Name)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/master/Add_Experiment_Type");
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(Name);

                    streamWriter.Write(json);
                }

                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(EmptyResponse));

                EmptyResponse obj = serializer.ReadObject(readStream.BaseStream) as EmptyResponse;

                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Create_Experiment_Type exp_list(dynamic Name)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/master/Get_Experiment_Types/" + "?AdminId=" + Name);
                httpWebRequest.Method = "GET";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";
                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(Create_Experiment_Type));
                Create_Experiment_Type obj = serializer.ReadObject(readStream.BaseStream) as Create_Experiment_Type;



                return obj;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }

        public EmptyResponse Create_Department(department data)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/master/Add_New_Department");
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(data);

                    streamWriter.Write(json);
                }

                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(EmptyResponse));

                EmptyResponse obj = serializer.ReadObject(readStream.BaseStream) as EmptyResponse;

                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Departments_data department_list(dynamic Name)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/master/Get_Department_List/" + "?AdminId=" + Name);
                httpWebRequest.Method = "GET";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";
                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(Departments_data));
                Departments_data obj = serializer.ReadObject(readStream.BaseStream) as Departments_data;



                return obj;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }

        public EmptyResponse Create_Admin(Create_admin data)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/master/Add_New_Admin");
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(data);

                    streamWriter.Write(json);
                }

                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(EmptyResponse));

                EmptyResponse obj = serializer.ReadObject(readStream.BaseStream) as EmptyResponse;

                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public User_information Get_UserList(string departmentId)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/master/Get_Admin_List?departmentId="+departmentId);
                httpWebRequest.Method = "GET";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";
                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());
                var serializer = new DataContractJsonSerializer(typeof(User_information));
                User_information obj = serializer.ReadObject(readStream.BaseStream) as User_information;
                return obj;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }

        public EmptyResponse Upload_product_files(string file_path, object login_id)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Campain/Add_Product_hierarchy/" + "?file_path=" + file_path + "&login_id=" + login_id);
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(file_path);

                    streamWriter.Write(json);
                }

                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(EmptyResponse));

                EmptyResponse obj = serializer.ReadObject(readStream.BaseStream) as EmptyResponse;

                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public EmptyResponse Delete_department(string department_id)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/master/department_Delete?department_id=" + department_id);
                httpWebRequest.Method = "DELETE";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";
                httpWebRequest.ContentLength = 0;

                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(EmptyResponse));

                EmptyResponse obj = serializer.ReadObject(readStream.BaseStream) as EmptyResponse;

                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public EmptyResponse Delete_Experiment(string Experiment_Id)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/master/Experiment_Delete?Experiment_id="+Experiment_Id);
                httpWebRequest.Method = "DELETE";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";
                httpWebRequest.ContentLength = 0;

                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(EmptyResponse));

                EmptyResponse obj = serializer.ReadObject(readStream.BaseStream) as EmptyResponse;

                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public EmptyResponse Delete_Validation(string Id)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/master/Validation_Delete?validation_id="+Id);
                httpWebRequest.Method = "DELETE";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";
                httpWebRequest.ContentLength = 0;

                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(EmptyResponse));

                EmptyResponse obj = serializer.ReadObject(readStream.BaseStream) as EmptyResponse;

                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public EmptyResponse Delete_Documents_Type(string Id)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/master/Document_Delete?document_id="+Id);
                httpWebRequest.Method = "DELETE";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";
                httpWebRequest.ContentLength = 0;

                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(EmptyResponse));

                EmptyResponse obj = serializer.ReadObject(readStream.BaseStream) as EmptyResponse;

                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
