﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Web.Script.Serialization;
using Models;

namespace BussinesLogic
{
    public class CampainBLL
    {
        public EmptyResponse Create_Campain(Create_Campain Name)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Campain/Create_New_Campain");
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(Name);

                    streamWriter.Write(json);
                }

                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(EmptyResponse));

                EmptyResponse obj = serializer.ReadObject(readStream.BaseStream) as EmptyResponse;

                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public EmptyResponse Create_Product_Hierarchy(product_hierarchy data)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Campain/Add_Product_hierarchy");
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(data);

                    streamWriter.Write(json);
                }

                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(EmptyResponse));

                EmptyResponse obj = serializer.ReadObject(readStream.BaseStream) as EmptyResponse;

                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public EmptyResponse Create_transaction(Create_transaction data)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Campain/Add_transaction_type");
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(data);

                    streamWriter.Write(json);
                }

                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(EmptyResponse));

                EmptyResponse obj = serializer.ReadObject(readStream.BaseStream) as EmptyResponse;

                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Get_Campain_Details Get_campains(string user_id)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Campain/Select_All_Campains?user_id="+user_id);
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";
                httpWebRequest.ContentLength = 0;

                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(Get_Campain_Details));

                Get_Campain_Details obj = serializer.ReadObject(readStream.BaseStream) as Get_Campain_Details;

                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        
        public EmptyResponse Add_Documents(Add_Documents data)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Campain/Add_Campain_Documents");
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(data);

                    streamWriter.Write(json);
                }

                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(EmptyResponse));

                EmptyResponse obj = serializer.ReadObject(readStream.BaseStream) as EmptyResponse;

                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Get_Campaign_Validaton_Details Get_campain_stages(string campaign_id)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Campain/Get_Campain_validation_stage?Campaign_id=" + campaign_id);
                httpWebRequest.Method = "GET";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";
                httpWebRequest.ContentLength = 0;

                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(Get_Campaign_Validaton_Details));

                Get_Campaign_Validaton_Details obj = serializer.ReadObject(readStream.BaseStream) as Get_Campaign_Validaton_Details;

                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Campain_Complete_Details Get_Campaign_details(string Id)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Campain/Select_Campain_Details?campaign_id="+Id);
                httpWebRequest.Method = "GET";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";
                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(Campaign_Details));
                Campaign_Details obj = serializer.ReadObject(readStream.BaseStream) as Campaign_Details;

                return obj.content;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }

        public EmptyResponse Rescore_campaign(Add_Documents data)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Campain/Add_scoring_Documents");
                httpWebRequest.Method = "POST";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(data);

                    streamWriter.Write(json);
                }

                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(EmptyResponse));

                EmptyResponse obj = serializer.ReadObject(readStream.BaseStream) as EmptyResponse;

                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ScoringData[] Get_Campain_Excel(string Id)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Campain/Get_Campain_Excel?Campaign_id=" + Id);
                httpWebRequest.Method = "GET";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";
                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(ScoringData[]));
                ScoringData[] obj = serializer.ReadObject(readStream.BaseStream) as ScoringData[];

                return obj;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }

        public Visual_Data[] Get_scoring_summary(string campaign_Id,string category)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Campain/Get_Scoring_summary?Campaign_id="+campaign_Id+"&category=" +category);
                httpWebRequest.Method = "GET";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";
                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(Visual_Data[]));
                Visual_Data[] obj = serializer.ReadObject(readStream.BaseStream) as Visual_Data[];

                return obj;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }


        public cross_sell[] Get_cross_sell(string campaign_Id)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Campain/Get_cross_sell?Campaign_id=" + campaign_Id);
                httpWebRequest.Method = "GET";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";
                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(cross_sell[]));
                cross_sell[] obj = serializer.ReadObject(readStream.BaseStream) as cross_sell[];

                return obj;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }


        public summary_stats[] Get_summary_stat(string campaign_Id)
        {
            try
            {
                StreamReader readStream;
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(@"http://" + General.Service_Link + "/api/Campain/Get_Summary_stats?Campaign_id=" + campaign_Id);
                httpWebRequest.Method = "GET";
                httpWebRequest.ContentType = @"application/json; charset=utf-8";
                HttpWebResponse httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                readStream = new StreamReader(httpResponse.GetResponseStream());

                var serializer = new DataContractJsonSerializer(typeof(summary_stats[]));
                summary_stats[] obj = serializer.ReadObject(readStream.BaseStream) as summary_stats[];

                return obj;
            }
            catch (Exception e)
            {
                Console.Write(e);
                return null;
            }
        }
    }
}
