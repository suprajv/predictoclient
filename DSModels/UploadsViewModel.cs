﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Models
{
    public class UploadsViewModel
    {
        public long ID { get; set; }
        public List<File1> Uploads { get; set; }

        public UploadsViewModel()
        {
            this.Uploads = new List<File1>();
        }
    }


    public class File1
    {
        public string DocType { get; set; }
        public string DocTypeId { get; set; }
        public string FilePath { get; set; }
        public int Docname{ get; set; }
        public long FileID { get; set; }
        public string FileName { get; set; }
    }
}