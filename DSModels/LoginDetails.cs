﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Models
{
    public class Login
    {
        public string User_Id { get; set; }
        
        public string Password { get; set; }
    }

    public class Login_content
    {
        public int id { get; set; }
        public string login_id { get; set; }
        public string password { get; set; }
        public string created_date { get; set; }
    }

    public class Login_Response
    {
        public int status { get; set; }

        public string message { get; set; }

        public Login_content content { get; set; }
    }
}