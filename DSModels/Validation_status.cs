﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Models
{
    public class Validation_Status
    {
        public int Id { get; set; }
        
        public string Name { get; set; }

        public string Message { get; set; }
    }

    public class EmptyResponse
    {
        public int status { get; set; }
        public string message { get; set; }
        public string content { get; set; }
    }
}