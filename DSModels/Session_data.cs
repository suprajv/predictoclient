﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Session_data
    {
        public string user_id { get; set; }
        public string user_name { get; set; }
        public int role_id { get; set; }
    }
}
