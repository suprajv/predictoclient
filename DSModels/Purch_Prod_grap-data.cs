﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Purch_Prod_grap_data
    {

        public Purch_Prod_grap_data()
        {

        }

        public string Age_Bucket
        {
            get { return _Age_Bucket; }
            set { _Age_Bucket = value; }
        }
        private string _Age_Bucket;

        public string Frequency
        {
            get { return _Frequency;}
            set { _Frequency = value; }
         }
        private string _Frequency;
        public Purch_Prod_grap_data(
            string Age_Bucket,
            string Frequency
            )
        {
            this._Age_Bucket = Age_Bucket;
            this._Frequency = Frequency;
        }
 
    }
}
