﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
   public  class Matrix_viewdata
    {

        public Matrix_viewdata()
        {

        }

        public int bin
        {
            get { return _bin; }
            set { _bin = value; }
        }
        private int _bin;

        public double avg
        {
            get { return _avg; }
            set { _avg = value; }
        }
        private double _avg;

        public Matrix_viewdata(
            int bin,
            double avg
            )
        {

            this._bin = bin;
            this._avg = avg;
        }

    }
}
