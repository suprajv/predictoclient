﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Create_Campain
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Experiment_type_id { get; set; }
        public string user_id { get; set; }
    }

    public class Documents_type
    {
        public string file_name { get; set; }

        public string file_path { get; set; }

        public string document_type_id { get; set; }
    }

    public class Campains_Details
    {
        
        public string Id { get; set; }

        public string Exp_Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Experiment_type { get; set; }

        public string Transaction_Id { get; set; }

        public string validation_status { get; set; }
        
        public string started_date { get; set; }

    }

    public class Campaign_Validaton_Details
    {
        public string Campaign_id { get; set; }

        public int validation_status { get; set; }

        public bool? Isfailure { get; set; }

        public string validation_message { get; set; }
    }

    public class Get_Campain_Details
    {
        public int status { get; set; }

        public string message { get; set; }

        public List<Campains_Details> content { get; set; }
    }

    public class Get_Campaign_Validaton_Details
    {
        public int status { get; set; }

        public string message { get; set; }

        public Campaign_Validaton_Details content { get; set; }
    }

    public class Add_Documents
    {
        public string campaign_id { get; set; }

        public List<Documents_type> odoc { get; set; }
    }
}
