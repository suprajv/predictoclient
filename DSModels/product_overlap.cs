﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class product_overlap
    {
        public product_overlap()
        { }
        public string Products
        {
            get { return _Products; }
            set { _Products = value; }
        }
        private string _Products;

        public string Product_Label
        {
            get { return _Product_Label; }
            set { _Product_Label = value; }
        }
        private string _Product_Label;


        public int size
        {
            get { return _size; }
            set { _size = value; }
        }
        private int _size;

        public product_overlap(
            string Products,
            string Product_Label,
            int size
            )
        {
            this._Products = Products;
            this._Product_Label = Product_Label;
            this._size = size;
        }

    }
}
