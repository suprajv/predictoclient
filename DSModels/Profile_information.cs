﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 
namespace Models
{
   public class Profile_information
    {
        public string id { get; set; }
        public string login_id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email_id { get; set; }
        public string phone_no { get; set; }
        public string created_by { get; set; }
        public string modified_by { get; set; }
        public string modified_date { get; set; }
        public string created_date { get; set; }
     
       public string Change_password { get; set; }
    }
}
