﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Product_Purchase_campains
    {

        public Product_Purchase_campains()
        {

        }


        public string Campain_id
        {
            get { return _Campain_id; }
            set { _Campain_id = value; }
        }
        private string _Campain_id;

        public string Campain_name
        {
            get { return _Campain_name; }
            set { _Campain_name = value; }
        }
        private string _Campain_name;

        public Product_Purchase_campains(
            string Campain_id,
            string Campain_name
            )
        {
            this._Campain_id = Campain_id;
            this._Campain_name = Campain_name;
        }

    
    
    }
}
