﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Models
{
    public class Document_Type
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }

    public class Rescore
    {
        public Document_Type[] odocument { get; set; }

        public string campaign_id { get; set; }

    }

    public class User_information_details
    {

        public int status { get; set; }
        public string message { get; set; }
        public List<Profile_information> content { get; set; }
    }


    public class User_information
    {
        public int status { get; set; }
        public string message { get; set; }
        public List<user_list> content { get; set; }
    }
    public class Session_values
    {
        public int status { get; set; }
        public string message { get; set; }
        public Session_data content { get; set; }
    }

    public class Departments_data
    {
        public int status { get; set; }
        public string message { get; set; }
        public List<Department_list> content { get; set; }
        public Create_admin admin_create { get; set; }
    }


    public class Product_Purchaselist
    {
         public Purchase_Products[] product_list { get; set; }
            
            public string Campain_id { get; set; }
            public string product_name { get; set; }
            public Product_Purchase_campains [] content { get; set; }
           
        

    }


    public class insert_exp_type
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<Document_Type> Document_id { get; set; }
        public int status { get; set; }
        public string[] SelectedValues { get; set; }

        public string message { get; set; }
        public List<Document_Type> content { get; set; }
    }

    public class Insert_exp
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<string> Document_id { get; set; }
    }


    public class Experiment_Type_list
    {
        public int status { get; set; }
        public string message { get; set; }

    }

    public class Campain_insert
    {
        public string Id { get; set; }
        public List<Select_exp_type> content { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Experiment_type_id { get; set; }
        public List<string> Document_id { get; set; }
    }

    public class Overlap
    {
        public List<string> sets { get; set; }
        public int size { get; set; }
        public string label { get; set; }
    }

    public class Create_Experiment_Type
    {
        public List<Select_exp_type> content { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }

        public List<string> Document_id { get; set; }
    }




    public class insert_document
    {
        public string Id { get; set; }

        public string Document_name { get; set; }
    }


    public class transaction_data
    {
        public string Id { get; set; }
        public string target_id { get; set; }
        public string source_id { get; set; }
        public List<string> otargetlist { get; set; }
        public List<string> osourcelist { get; set; }
        public int status { get; set; }
        public string message { get; set; }
        public List<Document_Type> content { get; set; }
        public Create_transaction create_trans { get; set; }
    }
    public class document_list
    {
        public int status { get; set; }
        public string message { get; set; }
        public List<Document_Type> content { get; set; }
    }

    public class Campaign_Validation_Stage
    {
        public string campaign_id { get; set; }
        public int stage { get; set; }
        public bool? IsFailure { get; set; }
        public string validation_message { get; set; }
        public Validation_Status[] ostatus { get; set; }
    }

    public class Campain_Details
    {
        public Create_Campain oBasicDetails { get; set; }
        public transaction_data oTransactionDetails { get; set; }
    }

    public class Camapin_Steps
    {
        public int Step { get; set; }
        public List<Document_Type> otype { get; set; }
    }

    public class Visual_Data
    {
        public Visual_Data()
        {

        }

        public string target { get; set; }

        public string column { get; set; }

        public string bin { get; set; }

        public string frequency { get; set; }

        public Visual_Data(
            string target,
            string column,
            string bin,
            string frequency
            )
        {
            this.target = target;
            this.column = column;
            this.bin = bin;
            this.frequency = frequency;
        }
    }

    public class ScoringData
    {
        public string CIF { get; set; }
        public string TITLE { get; set; }
        public string HOME_BRANCH { get; set; }
        public string HOST_ID { get; set; }
        public string NATIONALITY { get; set; }

        public string GENDER { get; set; }
        public string BIRTH_DT { get; set; }
        public string CID_BD { get; set; }
        public string AGE { get; set; }
        public string CUST_CREATION_DATE { get; set; }
        public string RELSHIP_YEARS { get; set; }
        public string VIP_CD { get; set; }
        public string VIP_DESC { get; set; }
        public string EMPL_CATEGORY { get; set; }
        public string HAS_CARD { get; set; }
        public string NBR_CARDS { get; set; }
        public string HAS_LOAN { get; set; }
        public string LOANS_ORGAMT { get; set; }
        public string LOANS_VAL { get; set; }

        public string NBR_LOANS { get; set; }
        public string CUST_TYPE { get; set; }
        public string CUST_SEGMENT { get; set; }
        public string PROFESSION { get; set; }
        public string HAS_FUND { get; set; }
        public string NBR_FUNDS { get; set; }
        public string FUND_VAL { get; set; }
        public string CUST_DEP_VAL { get; set; }
        public string CUST_SELECTION { get; set; }
        public string INCOME { get; set; }
        public string prediction { get; set; }

    }

    public class Dashboard
    {
        public Visual_Data[] odata { get; set; }

        public Campain_count[] odetails { get; set; }

        public string[] ocolumn { get; set; }

        public string[] otarget { get; set; }

        public string ocolumn_id { get; set; }
        
        public string otarget_id { get; set; }
    }

    public class d_graph_data
    {
        public string Product { get; set; }
        public decimal Count { get; set; }
    }
    

    public class g_matrix_data
    {

        public int Product { get; set; }
        public decimal Count { get; set; }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class g_overlap_graph
    {
        [JsonProperty]
        public string sets { get; set; }
        [JsonProperty]
        public string label { get; set; }
        [JsonProperty]
        public int size { get; set; }

    }

    public class Dashboard_Details
    {
        public int status { get; set; }
        public string message { get; set; }
        public Dashboard content { get; set; }
    }

    public class Campain_count
    {
        public string validation_status { get; set; }

        public int status { get; set; }
    }

    public class Line_Data
    {
        public Line_Data()
        {

        }

        public string bucket { get; set; }

        public decimal actual { get; set; }

        public decimal predicted { get; set; }

        public Line_Data(
            string bucket,
            decimal actual,
            decimal predicted
            )
        {
            this.bucket = bucket;
            this.actual = actual;
            this.predicted = predicted;
        }
    }

    public class Bubble_Data
    {
        public Bubble_Data()
        {

        }

        public string bucket { get; set; }

        public string column { get; set; }

        public int value { get; set; }

        public Bubble_Data(
            string bucket,
            string column,
            int value
            )
        {
            this.bucket = bucket;
            this.column = column;
            this.value = value;
        }
    }

    public class Bubble_Filters
    {
        public string[] ofilters { get; set; }

        public string selected_filter { get; set; }

    }


    public class Campain_Complete_Details
    {
        public string Id { get; set; }

        public string Exp_Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Experiment_type { get; set; }

        public string Source_Type { get; set; }

        public string Destination_Type { get; set; }

        public string Validation_stage { get; set; }

        public List<Doc_Info> Documents_List { get; set; }

        public List<string> Category_List { get; set; }
        
        public bool Is_Scoring { get; set; }

    }

    public class Campaign_Details
    {
        public int status { get; set; }
        public string message { get; set; }
        public Campain_Complete_Details content { get; set; }
    }

    public class Doc_Info
    {
        public string type { get; set; }
        public string name { get; set; }
    }

    public class Campaign_Excel
    {
        public int status { get; set; }
        public string message { get; set; }
        public DataTable content { get; set; }
    }

    public class cross_sell
    {
        public string month { get; set; }
        public string percent { get; set; }
        public string source { get; set; }
        public string destination { get; set; }
    }

    public class summary_stats
    {
        public string month { get; set; }
        public string trans_type { get; set; }
        public string property { get; set; }
        public string property_value { get; set; }
        public string rolling_mean { get; set; }
        public string rolling_std_dev { get; set; }
        public string calc_value { get; set; }
    }
}